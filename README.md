## This project belongs to a complete DevOps bootcamp

[👉 Techworld with Nana DevOps program](https://www.techworld-with-nana.com/devops-bootcamp)

### Demo Project:
Automate Node.js application Deployment
### Technologies used:
Ansible, Node.js, DigitalOcean, Linux
### Project Description:
- Create Server on DigitalOcean
- Write Ansible Playbook that installs necessary
technologies, creates Linux user for an application and
deploys a NodeJS application with that user

---

### Demo Project:
Ansible & Docker
### Technologies used:
Ansible, AWS, Docker, Terraform, Linux
### Project Description:
- Create AWS EC2 Instance with Terraform
- Write Ansible Playbook that installs necessary
technologies like Docker and Docker Compose, copies
docker-compose file to the server and starts the
Docker containers configured inside the dockercompose file

---

### Demo Project:
Configure Dynamic Inventory
### Technologies used:
Ansible, Terraform, AWS
### Project Description:
- Create EC2 Instance with Terraform
- Write Ansible AWS EC2 Plugin to dynamically sets
inventory of EC2 servers that Ansible should manage,
instead of hard-coding server addresses in Ansible
inventory file

---

### Demo Project:
Automate Nexus Deployment
### Technologies used:
Ansible, Nexus, DigitalOcean, Java, Linux
### Project Description:
- Create Server on DigitalOcean
- Write Ansible Playbook that creates Linux user for Nexus,
configure server, installs and deploys Nexus and verifies
that it is running successfully

---

### Demo Project:
Ansible Integration in Terraform
### Technologies used:
Ansible, Terraform, AWS, Docker, Linux
### Project Description:
- Create Ansible Playbook for Terraform integration
- Adjust Terraform configuration to execute Ansible
Playbook automatically, so once Terraform provisions a
server, it executes an Ansible playbook that configures
the server

---

### Demo Project:
Automate Kubernetes Deployment
### Technologies used:
Ansible, Terraform, Kubernetes, AWS EKS, Python, Linux
### Project Description:
- Create EKS cluster with Terraform
- Write Ansible Play to deploy application in a new K8s
namespace

---

### Demo Project:
Ansible Integration in Jenkins
### Technologies used:
Ansible, Jenkins, DigitalOcean, AWS, Boto3, Docker, Java, Maven, Linux, Git
### Project Description:
- Create and configure a dedicated server for Jenkins
- Create and configure a dedicated server for Ansible Control Node
- Write Ansible Playbook, which configures 2 EC2 Instances
- Add ssh key file credentials in Jenkins for Ansible Control Node server and Ansible Managed Node
servers
- Configure Jenkins to execute the Ansible Playbook on remote Ansible Control Node server as
part of the CI/CD pipeline
- The Jenkinsfile configuration will do the following:
    a. Connect to the remote Ansible Control Node server
    b. Copy Ansible playbook and configuration files to the remote Ansible Control Node server
    c. Copy the ssh keys for the Ansible Managed Node servers to the Ansible Control Node server
    d. Install Ansible, Python3 and Boto3 on the Ansible Control Node server
    e. With everything installed and copied to the remote Ansible Control Node server, execute the
    playbook remotely on that Control Node that will configure the 2 EC2 Managed Nodes

---

### Demo Project:
Structure Playbooks with Ansible Roles
### Technologies used:
Ansible, Docker, AWS, Linux
### Project Description:
- Break up large Ansible Playbooks into smaller manageable files using Ansible Roles

---
__Module 15: Configuration Management with Ansible__